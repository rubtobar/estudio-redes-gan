import sys
sys.path.append('..')

import numpy as np
import os

data_dir = '../data/Kuzushiji-MNIST/'
def mnist():
#   [offset] [type]          [value]          [description] 
#   0000     32 bit integer  0x00000803(2051) magic number 
#   0004     32 bit integer  60000            number of images 
#   0008     32 bit integer  28               number of rows 
#   0012     32 bit integer  28               number of columns 
#   0016     unsigned byte   ??               pixel 
    fd = open(os.path.join(data_dir,'train-images-idx3-ubyte'))
    loaded = np.fromfile(file=fd,dtype=np.uint8) # Tamaño de uint8 = 1 byte
    trX = loaded[16:].reshape((60000,28*28)).astype(float) # Nos saltamos hasta el valor 16

#   [offset] [type]          [value]          [description] 
#   0000     32 bit integer  0x00000801(2049) magic number (MSB first) 
#   0004     32 bit integer  60000            number of items 
#   0008     unsigned byte   ??               label 
    fd = open(os.path.join(data_dir,'train-labels-idx1-ubyte'))
    loaded = np.fromfile(file=fd,dtype=np.uint8) # Tamaño de uint8 = 1 byte
    trY = loaded[8:].reshape((60000)) # Nos saltamos hasta el valor 8

    # Misma operacion para test set
    fd = open(os.path.join(data_dir,'t10k-images-idx3-ubyte'))
    loaded = np.fromfile(file=fd,dtype=np.uint8)
    teX = loaded[16:].reshape((10000,28*28)).astype(float)

    fd = open(os.path.join(data_dir,'t10k-labels-idx1-ubyte'))
    loaded = np.fromfile(file=fd,dtype=np.uint8)
    teY = loaded[8:].reshape((10000))

    trY = np.asarray(trY)
    teY = np.asarray(teY)

    return trX, teX, trY, teY

def mnist_with_valid_set():
    trX, teX, trY, teY = mnist()

    train_inds = np.arange(len(trX))
    np.random.shuffle(train_inds)
    trX = trX[train_inds]
    trY = trY[train_inds]
    #trX, trY = shuffle(trX, trY)
    vaX = trX[50000:]
    vaY = trY[50000:]
    trX = trX[:50000]
    trY = trY[:50000]

    return trX, vaX, teX, trY, vaY, teY
