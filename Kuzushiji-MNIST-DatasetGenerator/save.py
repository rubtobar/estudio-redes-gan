import sys
sys.path.append('..')

import numpy as np
import os

data_dir = '../data/Kuzushiji-MNIST-gen/'
def save_mnist(images, labels):

    # Convertimos los valores de cada pixel a bytes
    images = images.astype(np.uint8)
    labels = labels.astype(np.uint8)

    write_imagedata(images, os.path.join(data_dir,'train-images-idx3-ubyte'))

    write_labeldata(labels, os.path.join(data_dir,'train-labels-idx1-ubyte'))


def write_imagedata(imagedata, outputfile):
#---[offset]-[type]----------[value]----------[description]
#---0000-----32-bit-integer--0x00000803(2051)-magic-number
#---0004-----32-bit-integer--60000------------number-of-images
#---0008-----32-bit-integer--28---------------number-of-rows
#---0012-----32-bit-integer--28---------------number-of-columns
#---0016-----unsigned-byte---??---------------pixel
  header = np.array([0x0803, 60000, 28, 28], dtype='>i4')
  if not os.path.exists(outputfile):
      with open(outputfile, "wb") as f:
        f.write(header.tobytes())
        f.write(imagedata.tobytes())
  else:
      with open(outputfile, "ab") as f:
        f.write(imagedata.tobytes())




def write_labeldata(labeldata, outputfile):
#---[offset]-[type]----------[value]----------[description]
#---0000-----32 bit integer--0x00000801(2049)-magic number (MSB first)
#---0004-----32 bit integer--60000------------number of items
#---0008-----unsigned byte---??---------------label
  header = np.array([0x0801, 60000], dtype='>i4')
  if not os.path.exists(outputfile):
      with open(outputfile, "wb") as f:
        f.write(header.tobytes())
        f.write(labeldata.tobytes())
  else:
      with open(outputfile, "ab") as f:
        f.write(labeldata.tobytes())