# Based on MNIST CNN from Keras' examples: https://github.com/keras-team/keras/blob/master/examples/mnist_cnn.py (MIT License)

from __future__ import print_function
import tensorflow.keras as keras
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Flatten
from tensorflow.keras.layers import Conv2D, MaxPooling2D
from tensorflow.keras import backend as K
import numpy as np
from load import mnist
from sklearn.metrics import confusion_matrix
import pandas as pd


batch_size = 128
num_classes = 10
epochs = 12

# input image dimensions
img_rows, img_cols = 28, 28

# def load(f):
#     return np.load(f)['arr_0']


paths = ['../Digits-MNIST', '../Digits-MNIST-gen',
         '../Kuzushiji-MNIST', '../Kuzushiji-MNIST-gen']

arch = open("results.txt", "w")

arch.write('*****************************************************************************\n')
arch.write('***************************** Start Test ************************************\n')
arch.write('*****************************************************************************\n')

for path in paths:

    # Load the data
    x_train, x_test, y_train, y_test = mnist(path)
    y_test_values = y_test

    if K.image_data_format() == 'channels_first':
        x_train = x_train.reshape(x_train.shape[0], 1, img_rows, img_cols)
        x_test = x_test.reshape(x_test.shape[0], 1, img_rows, img_cols)
        input_shape = (1, img_rows, img_cols)
    else:
        x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 1)
        x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, 1)
        input_shape = (img_rows, img_cols, 1)

    x_train = x_train.astype('float32')
    x_test = x_test.astype('float32')
    x_train /= 255
    x_test /= 255
    print('{} train samples, {} test samples'.format(len(x_train), len(x_test)))

    # Convert class vectors to binary class matrices
    y_train = keras.utils.to_categorical(y_train, num_classes)
    y_test = keras.utils.to_categorical(y_test, num_classes)

    model = Sequential()
    model.add(Conv2D(32, kernel_size=(3, 3),
                     activation='relu',
                     input_shape=input_shape))
    model.add(Conv2D(64, (3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))
    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(num_classes, activation='softmax'))

    model.compile(loss=keras.losses.categorical_crossentropy,
                  optimizer=keras.optimizers.Adadelta(),
                  metrics=['accuracy'])

    model.fit(x_train, y_train,
              batch_size=batch_size,
              epochs=epochs,
              verbose=1,
              validation_data=(x_test, y_test))

    train_score = model.evaluate(x_train, y_train, verbose=0)
    test_score = model.evaluate(x_test, y_test, verbose=0)
    

    y_pred = model.predict(x_test)

    y_pred = np.argmax(y_pred, axis=1)

    cnf_matrix = confusion_matrix(y_true=y_test_values, y_pred=y_pred)

    FP = cnf_matrix.sum(axis=0) - np.diag(cnf_matrix)
    FN = cnf_matrix.sum(axis=1) - np.diag(cnf_matrix)
    TP = np.diag(cnf_matrix)
    TN = cnf_matrix.sum() - (FP + FN + TP)

    FP = FP.astype(float)
    FN = FN.astype(float)
    TP = TP.astype(float)
    TN = TN.astype(float)

    clases = np.arange(0, 10)

    # Sensitivity, hit rate, recall, or true positive rate
    recall = TP/(TP+FN)
    # Specificity or true negative rate
    specify = TN/(TN+FP)
    # Fall out or false positive rate
    FPR = FP/(FP+TN)
    # False negative rate
    FNR = FN/(TP+FN)
    # Percentage of Wrong Classifications
    PWC = 100 * (FN + FP) / (TP + FN + FP + TN)
    # Precision
    precision = TP / (TP + FP)
    # f-measure
    F_Measure = (2 * precision * recall) / (precision + recall)

    summary = np.concatenate(
        (clases, recall, specify, FPR, FNR, PWC, precision, F_Measure))

    summary = summary.reshape([8, 10])

    headers = (['class', 'recall', 'specify', 'FPR',
                'FNR', 'PWC', 'precision', 'F-Measure'])

    dataframe = pd.DataFrame(summary.transpose(), columns=headers)

    arch.write('-----------------------------------------------------------------------------\n')
    arch.write('Testing dataset in: ' + path + '\n')
    arch.write('Train loss:' + str(train_score[0]) + '\n')
    arch.write('Train accuracy:' + str(train_score[1]) + '\n')
    arch.write('Test loss:' + str(test_score[0]) + '\n')
    arch.write('Test accuracy:' + str(test_score[1]) + '\n')
    arch.write(str(dataframe))
    arch.write('\n--------------------------------------------------------------------------\n')


arch.write('\n*****************************************************************************\n')
arch.write('***************************** End Test ************************************\n')
arch.write('*****************************************************************************\n\n\n\n')
arch.close()