*****************************************************************************
***************************** Start Test ************************************
*****************************************************************************
-----------------------------------------------------------------------------
Testing dataset in: ../Digits-MNIST
Train loss:0.00830990064855129
Train accuracy:0.99755
Test loss:0.028896999723680802
Test accuracy:0.9916
   class    recall   specify       FPR       FNR   PWC  precision  F-Measure
0    0.0  0.997959  0.998004  0.001996  0.002041  0.20   0.981928   0.989879
1    1.0  0.999119  0.999210  0.000790  0.000881  0.08   0.993865   0.996485
2    2.0  0.988372  0.999108  0.000892  0.011628  0.20   0.992218   0.990291
3    3.0  0.994059  0.999444  0.000556  0.005941  0.11   0.995045   0.994552
4    4.0  0.994908  0.999224  0.000776  0.005092  0.12   0.992886   0.993896
5    5.0  0.988789  0.999671  0.000329  0.011211  0.13   0.996610   0.992684
6    6.0  0.990605  0.999447  0.000553  0.009395  0.14   0.994759   0.992678
7    7.0  0.987354  0.998885  0.001115  0.012646  0.23   0.990244   0.988797
8    8.0  0.986653  0.999114  0.000886  0.013347  0.21   0.991744   0.989192
9    9.0  0.987116  0.998554  0.001446  0.012884  0.26   0.987116   0.987116
--------------------------------------------------------------------------
-----------------------------------------------------------------------------
Testing dataset in: ../Digits-MNIST-gen
Train loss:0.0013315687113996698
Train accuracy:0.99965
Test loss:0.22369438460441599
Test accuracy:0.9536
   class    recall   specify       FPR       FNR   PWC  precision  F-Measure
0    0.0  0.992857  0.990022  0.009978  0.007143  0.97   0.915334   0.952521
1    1.0  0.989427  0.997744  0.002256  0.010573  0.32   0.982502   0.985953
2    2.0  0.923450  0.997324  0.002676  0.076550  1.03   0.975435   0.948731
3    3.0  0.965347  0.993103  0.006897  0.034653  0.97   0.940212   0.952614
4    4.0  0.964358  0.995897  0.004103  0.035642  0.72   0.962398   0.963377
5    5.0  0.965247  0.991217  0.008783  0.034753  1.11   0.914984   0.939444
6    6.0  0.972860  0.994913  0.005087  0.027140  0.72   0.952965   0.962810
7    7.0  0.962062  0.995653  0.004347  0.037938  0.78   0.962062   0.962062
8    8.0  0.858316  0.998227  0.001773  0.141684  1.54   0.981221   0.915663
9    9.0  0.938553  0.994439  0.005561  0.061447  1.12   0.949850   0.944167
--------------------------------------------------------------------------
-----------------------------------------------------------------------------
Testing dataset in: ../Kuzushiji-MNIST
Train loss:0.011625185107097301
Train accuracy:0.9973666666666666
Test loss:0.21467033035950736
Test accuracy:0.9499
   class  recall   specify       FPR    FNR   PWC  precision  F-Measure
0    0.0   0.969  0.995778  0.004222  0.031  0.69   0.962264   0.965620
1    1.0   0.925  0.997222  0.002778  0.075  1.00   0.973684   0.948718
2    2.0   0.880  0.996222  0.003778  0.120  1.54   0.962801   0.919540
3    3.0   0.984  0.989111  0.010889  0.016  1.14   0.909427   0.945245
4    4.0   0.937  0.992000  0.008000  0.063  1.35   0.928642   0.932802
5    5.0   0.939  0.997667  0.002333  0.061  0.82   0.978125   0.958163
6    6.0   0.975  0.992222  0.007778  0.025  0.95   0.933014   0.953545
7    7.0   0.958  0.997444  0.002556  0.042  0.65   0.976555   0.967188
8    8.0   0.979  0.991222  0.008778  0.021  1.00   0.925331   0.951409
9    9.0   0.953  0.995444  0.004556  0.047  0.88   0.958753   0.955868
--------------------------------------------------------------------------
-----------------------------------------------------------------------------
Testing dataset in: ../Kuzushiji-MNIST-gen
Train loss:0.00017203220515743094
Train accuracy:1.0
Test loss:2.005791583633423
Test accuracy:0.724
   class  recall   specify       FPR    FNR   PWC  precision  F-Measure
0    0.0   0.778  0.986222  0.013778  0.222  3.46   0.862528   0.818086
1    1.0   0.756  0.974444  0.025556  0.244  4.74   0.766734   0.761329
2    2.0   0.753  0.927111  0.072889  0.247  9.03   0.534422   0.625156
3    3.0   0.677  0.988889  0.011111  0.323  4.23   0.871300   0.761958
4    4.0   0.671  0.963111  0.036889  0.329  6.61   0.668993   0.669995
5    5.0   0.789  0.972667  0.027333  0.211  4.57   0.762319   0.775430
6    6.0   0.794  0.941111  0.058889  0.206  7.36   0.599698   0.683305
7    7.0   0.561  0.987778  0.012222  0.439  5.49   0.836066   0.671454
8    8.0   0.733  0.968111  0.031889  0.267  5.54   0.718627   0.725743
9    9.0   0.728  0.983889  0.016111  0.272  4.17   0.833906   0.777363
--------------------------------------------------------------------------

*****************************************************************************
***************************** End Test ************************************
*****************************************************************************



