from __future__ import print_function
import tensorflow.keras as keras
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Flatten
from tensorflow.keras.layers import Conv2D, MaxPooling2D
from tensorflow.keras import backend as K
import numpy as np
from load import mnist
from sklearn.metrics import confusion_matrix
import pandas as pd
import matplotlib.pyplot as plt
from tqdm import tqdm


batch_size = 128
num_classes = 10
epochs = 12

# input image dimensions
img_rows, img_cols = 28, 28

# def load(f):
#     return np.load(f)['arr_0']


paths = ['../Digits-MNIST', '../Digits-MNIST-gen',
         '../Kuzushiji-MNIST', '../Kuzushiji-MNIST-gen']

figuresPaths = ['D:/wrong-predictions-convolutional/digits/', 'D:/wrong-predictions-convolutional/digits-gen/',
                'D:/wrong-predictions-convolutional/kuzushiji/', 'D:/wrong-predictions-convolutional/kuzushiji-gen/']

figuresPath = 0

for path in paths:

    # Load the data
    x_train, x_test, y_train, y_test = mnist(path)
    x_test_values = x_test
    y_test_values = y_test

    if K.image_data_format() == 'channels_first':
        x_train = x_train.reshape(x_train.shape[0], 1, img_rows, img_cols)
        x_test = x_test.reshape(x_test.shape[0], 1, img_rows, img_cols)
        input_shape = (1, img_rows, img_cols)
    else:
        x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 1)
        x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, 1)
        input_shape = (img_rows, img_cols, 1)

    x_train = x_train.astype('float32')
    x_test = x_test.astype('float32')
    x_train /= 255
    x_test /= 255
    print('{} train samples, {} test samples'.format(len(x_train), len(x_test)))

    # Convert class vectors to binary class matrices
    y_train = keras.utils.to_categorical(y_train, num_classes)
    y_test = keras.utils.to_categorical(y_test, num_classes)

    model = Sequential()
    model.add(Conv2D(32, kernel_size=(3, 3),
                     activation='relu',
                     input_shape=input_shape))
    model.add(Conv2D(64, (3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))
    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(num_classes, activation='softmax'))

    model.compile(loss=keras.losses.categorical_crossentropy,
                  optimizer=keras.optimizers.Adadelta(),
                  metrics=['accuracy'])

    model.fit(x_train, y_train,
              batch_size=batch_size,
              epochs=epochs,
              verbose=1,
              validation_data=(x_test, y_test))

    #train_score = model.evaluate(x_train, y_train, verbose=0)
    #test_score = model.evaluate(x_test, y_test, verbose=0)
    

    y_pred = model.predict(x_test)

    y_pred = np.argmax(y_pred, axis=1)

    #cnf_matrix = confusion_matrix(y_true=y_test_values, y_pred=y_pred)

    maxrange = 150

    if sum(sum([y_test_values != y_pred])) < 150:
    	maxrange = sum(sum([y_test_values != y_pred]))

    print(maxrange)

    for index in tqdm(range(maxrange)):

        plt.imshow(x_test_values[y_test_values != y_pred][index].reshape((28, 28)), cmap='gray')
        plt.savefig(figuresPaths[figuresPath]
                    + "Predicted " + str(y_test_values[y_test_values != y_pred][index])
                    + " as " + str(y_pred[y_test_values != y_pred][index])
                    + "(" + str(index) + ").png")

    figuresPath += 1


