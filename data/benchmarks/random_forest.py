from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix
import numpy as np
import pandas as pd
from load import mnist

clf = RandomForestClassifier()

paths = ['../Digits-MNIST', '../Digits-MNIST-gen',
         '../Kuzushiji-MNIST', '../Kuzushiji-MNIST-gen']

arch = open("results_randomforest.txt", "w")

arch.write('*****************************************************************************\n')
arch.write('***************************** Start Test ************************************\n')
arch.write('*****************************************************************************\n')

for path in paths:

    trX, teX, trY, teY = mnist(path)

    clf.fit(trX, trY)


    pred = clf.predict(teX)

    cnf_matrix = confusion_matrix(teY,pred)

    # Get measures
    FP = cnf_matrix.sum(axis=0) - np.diag(cnf_matrix)
    FN = cnf_matrix.sum(axis=1) - np.diag(cnf_matrix)
    TP = np.diag(cnf_matrix)
    TN = cnf_matrix.sum() - (FP + FN + TP)
    FP = FP.astype(float)
    FN = FN.astype(float)
    TP = TP.astype(float)
    TN = TN.astype(float)
    clases = np.arange(0, 10)
    # Sensitivity, hit rate, recall, or true positive rate
    recall = TP/(TP+FN)
    # Specificity or true negative rate
    specify = TN/(TN+FP)
    # Fall out or false positive rate
    FPR = FP/(FP+TN)
    # False negative rate
    FNR = FN/(TP+FN)
    # Percentage of Wrong Classifications
    PWC = 100 * (FN + FP) / (TP + FN + FP + TN)
    # Precision
    precision = TP / (TP + FP)
    # f-measure
    F_Measure = (2 * precision * recall) / (precision + recall)

    summary = np.concatenate(
        (clases, recall, specify, FPR, FNR, PWC, precision, F_Measure))

    summary = summary.reshape([8, 10])

    headers = (['class', 'recall', 'specify', 'FPR',
                'FNR', 'PWC', 'precision', 'F-Measure'])

    dataframe = pd.DataFrame(summary.transpose(), columns=headers)

    confusion = pd.DataFrame(cnf_matrix, columns=clases)

    arch.write('-----------------------------------------------------------------------------\n')
    arch.write('Testing dataset in: ' + path + '\n')
    arch.write(str(dataframe) + '\n')
    arch.write(str(confusion))
    arch.write('\n--------------------------------------------------------------------------\n')

arch.write('\n*****************************************************************************\n')
arch.write('***************************** End Test ************************************\n')
arch.write('*****************************************************************************\n\n\n\n')
arch.close()