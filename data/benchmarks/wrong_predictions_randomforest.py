from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix
import numpy as np
import pandas as pd
from load import mnist
import matplotlib.pyplot as plt
from tqdm import tqdm

clf = RandomForestClassifier()

paths = ['../Digits-MNIST', '../Digits-MNIST-gen',
         '../Kuzushiji-MNIST', '../Kuzushiji-MNIST-gen']

figuresPaths = ['D:/wrong-predictions-randforest/digits/', 'D:/wrong-predictions-randforest/digits-gen/',
                'D:/wrong-predictions-randforest/kuzushiji/', 'D:/wrong-predictions-randforest/kuzushiji-gen/']

figuresPath = 0

for path in paths:

    trX, teX, trY, teY = mnist(path)

    clf.fit(trX, trY)

    pred = clf.predict(teX)

    cnf_matrix = confusion_matrix(teY, pred)

    for index in tqdm(range(150)):

        plt.imshow(teX[teY != pred][index].reshape((28, 28)), cmap='gray')

        plt.savefig(figuresPaths[figuresPath]
                    + "Predicted " + str(teY[teY != pred][index])
                    + " as " + str(pred[teY != pred][index])
                    + "(" + str(index) + ").png")

        figuresPath += 1
